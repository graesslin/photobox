/********************************************************************
Copyright (C) 2017 Martin Gräßlin <mgraesslin@kde.org>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of
the License or (at your option) version 3 or any later version
accepted by the membership of KDE e.V. (or its successor approved
by the membership of KDE e.V.), which shall act as a proxy
defined in Section 14 of version 3 of the license.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
#include "gphotoprocess.h"

#include <QProcess>

#include <signal.h>

namespace PhotoBox
{
GphotoProcess::GphotoProcess(QObject*parent)
    : QObject(parent)
    , m_process(new QProcess(this))
{
}

GphotoProcess::~GphotoProcess()
{
    m_process->terminate();
    m_process->waitForFinished(5000);
}

void GphotoProcess::init()
{
    Q_ASSERT(!m_storageLocation.isEmpty());
    m_process->setProcessChannelMode(QProcess::ForwardedErrorChannel);
    m_process->setWorkingDirectory(m_storageLocation);

    m_process->setArguments(QStringList{
                                                QStringLiteral("--set-config"),
                                                QStringLiteral("output=TFT"),
                                                QStringLiteral("--capture-image-and-download"),
                                                QStringLiteral("--filename"),
                                                QStringLiteral("%Y%m%d-%H%M%S.jpg"),
                                                QStringLiteral("-I"),
                                                QStringLiteral("-1"),
                                                QStringLiteral("--reset-interval"),
                                                QStringLiteral("--skip-existing"),
    });
    m_process->setProgram(QStringLiteral("gphoto2"));
}

void GphotoProcess::takePhoto()
{
    if (m_process->state() == QProcess::NotRunning) {
        startProcess();
        return;
    }
    if (!m_timer.isValid() || !m_timer.hasExpired(2500)) {
        return;
    }
    ::kill(m_process->pid(), SIGUSR1);
    m_timer.restart();
}

void GphotoProcess::startProcess()
{
    m_process->start();
    m_process->waitForStarted(5000);
    m_timer.start();
}

}
