# Photobox

This application is intended to be used together with an external camera which can be controlled
through gphoto2. It triggers a photo on the external camera through PageUp and PageDown keys and
shows all photos in a slideshow.

Recommended usage: put to fullscreen through the window manager
(in case of KWin: Alt+F3, More Actions, Fullscreen).
