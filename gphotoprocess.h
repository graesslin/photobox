/********************************************************************
Copyright (C) 2017 Martin Gräßlin <mgraesslin@kde.org>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of
the License or (at your option) version 3 or any later version
accepted by the membership of KDE e.V. (or its successor approved
by the membership of KDE e.V.), which shall act as a proxy
defined in Section 14 of version 3 of the license.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
#ifndef PHOTOBOX_GPHOTOPROCESS_H
#define PHOTOBOX_GPHOTOPROCESS_H

#include <QObject>
#include <QElapsedTimer>

class QProcess;

namespace PhotoBox
{
class GphotoProcess : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString storageLocation READ storageLocation WRITE setStorageLocation)
public:
    explicit GphotoProcess(QObject *parent = nullptr);
    virtual ~GphotoProcess();

    void init();

    void setStorageLocation(const QString &storageLocation) {
        m_storageLocation = storageLocation;
    }
    QString storageLocation() const {
        return m_storageLocation;
    }

public Q_SLOTS:
    void takePhoto();

private:
    void startProcess();
    QProcess *m_process;
    QString m_storageLocation;
    QElapsedTimer m_timer;
};

}

#endif
