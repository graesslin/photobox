/********************************************************************
Copyright (C) 2017 Martin Gräßlin <mgraesslin@kde.org>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of
the License or (at your option) version 3 or any later version
accepted by the membership of KDE e.V. (or its successor approved
by the membership of KDE e.V.), which shall act as a proxy
defined in Section 14 of version 3 of the license.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/
#include "gphotoprocess.h"
#include "wallpaper_integration.h"

#include <QApplication>
#include <QQmlContext>
#include <QQuickView>
#include <QFileDialog>

#include <KConfigGroup>
#include <KDeclarative/KDeclarative>
#include <KDeclarative/QmlObjectSharedEngine>


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QString dir = QFileDialog::getExistingDirectory(nullptr, "Photobox storage location", "/home", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    if (dir.isEmpty()) {
        return 0;
    }

    QQuickView view;
    view.setResizeMode(QQuickView::SizeRootObjectToView);

    // first create KDeclarative, to be sure that it created a KIO Network Factory
    KDeclarative::KDeclarative declarative;
    declarative.setDeclarativeEngine(view.engine());
    declarative.setupBindings();

    PhotoBox::GphotoProcess process;
    process.setStorageLocation(dir);
    process.init();
    view.rootContext()->setContextProperty("photobox", &process);

    view.setSource(QUrl(QStringLiteral("qrc:/resources/main.qml")));
    view.show();

    // Wallpaper Integration
    PhotoBox::WallpaperIntegration *wallpaper = new PhotoBox::WallpaperIntegration(&process);
    auto config = KSharedConfig::openConfig(QString(), KConfig::SimpleConfig);
    config->group("PhotoBox").group("General").writeEntry("SlidePaths", QStringList(dir));
    config->group("PhotoBox").group("General").writeEntry("FillMode", 1);
    config->sync();
    wallpaper->setConfig(config);
    wallpaper->setPluginName("org.kde.slideshow");
    wallpaper->init();

    wallpaper->load(&view);
    if (auto object = view.property("wallpaperGraphicsObject").value<KDeclarative::QmlObjectSharedEngine*>()) {
        //initialize with our size to avoid as much resize events as possible
        object->completeInitialization({
            {QStringLiteral("width"), view.width()},
            {QStringLiteral("height"), view.height()}
        });
    }

    return app.exec();
}
